<?php

require 'ffile.php';

class task{

    protected $description;

    protected $completed = false;

    public function __construct($description)
    {

        $this ->description = $description;

    }

    public function complete()
    {

        $this -> completed = true;

    }



    public function isComplete()
    {

        return $this -> completed;

    }

}

$task = new task('go to the store');

$task -> complete();

var_dump($task -> isComplete());

