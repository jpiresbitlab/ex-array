<?php

class task{

    public $description;

    public $completed = false;

    public function __construct($description)
    {

        $this ->description = $description;

    }

    public function complete()
    {

        $this -> completed = true;

    }



    public function isComplete()
    {

        return $this -> completed;

    }

}

$tasks = [

new task('Go to the store'),
new task('Finish my screencast'),
new task('Clean my room')

];

$tasks[0]->complete();
require "class.view.php";
